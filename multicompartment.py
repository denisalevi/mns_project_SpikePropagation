import numpy as np
from scipy.signal import argrelextrema

# v is given in mV and alpha should be returned as s
alpha_n = lambda v: ( 0.01*(v*1e3+55.)/(1-np.exp(-0.1*(v*1e3+55.))) ) * 1e3
beta_n = lambda v: ( 0.125*np.exp(-0.0125*(v*1e3+65.)) ) * 1e3

alpha_m = lambda v: ( 0.1*(v*1e3+40.)/(1-np.exp(-0.1*(v*1e3+40.))) ) * 1e3
beta_m = lambda v: ( 4*np.exp(-0.0556*(v*1e3+65.)) ) * 1e3

alpha_h = lambda v: ( 0.07*np.exp(-0.05*(v*1e3+65.)) ) * 1e3
beta_h = lambda v: ( 1/(1+np.exp(-0.1*(v*1e3+35.))) ) * 1e3
    

class MultiCompartmentHodgkinHuxley(object):

    def __init__(self, N, a=238e-6, L=1e-2, rL=35.4e-2, cm=1e-2, Ie=[10e-9], mu_inj=[1],
            v0=-65e-3, gL=0.003e3, gK=0.36e3, gNa=1.2e3, EL=-54.387e-3, EK=-77e-3, ENa=50e-3, z=0.5,
            myelinated=False, len_ranvier=2e-6, len_myelin=1e-3):
            """
            :param N: (scalar) number of compartments
            :param a: (default=238.) cable radius [m] (scalar)
            :param L: (default=1.) total cable length [m] (scalar)
            :param rL: (default=35.4) intracellular resistivity [Ohm*m] (scalar)
            :param cm: (default=1.) specific membrane capacitance [F/(m^2)] (scalar)
            :param Ie: (default=10.) total external current (vector)
            :param mu_inj: (vector of int, default=0) compartment of current injection
            :param v0: (scalar, default=-65) initial membrane voltage
            :param gL: (scalar, default=0.3) leak conductance [nS]
            :param gK: (scalar, default=36.) maximum conductance of potassium channel [nS]
            :param gNa: (scalar, default=120.) maximum conductance of sodium channel [nS]
            :param EL: (scalar, default=-54.387) reversal potential of leakage [mV]
            :param EK: (scalar, default=-77.) reversal potential of potassium channel [mV]
            :param ENa: (scalar, default=50.) reversal potential of sodium channel [mV]
            :param z: (scalar, default=0.5) z=0.5 is Crank-Nicholson, z=1 is reverse Euler method
            :praram meyelin ...
            """
# TODO
            self.a = a
            self.L = L
            self.rL = rL
            self.cm = cm
            self.prev_cm = cm # hacky! for myelinated C and c calculation 
            self.Ie = np.array(Ie)
            self.mu_inj = np.array(mu_inj)
            self.N = N
            self.v0 = v0
            self.gL = gL
            self.gK = gK
            self.gNa = gNa
            self.EL = EL
            self.EK = EK
            self.ENa = ENa
            self.z = z

            self.l = np.sqrt( a / (2*rL* ( gL / (2 * np.pi * self.a * (self.L/self.N) * 1e-4)))) # electronic length constant lambda [
            self.anal_vel = np.sqrt(a/(2*cm**2*rL*(1./gL)))

            # injected current per unit are [nA/(cm^2)]:
            self.ie = np.zeros(self.N)
            self.S = (2 * np.pi * self.a * self.L/self.N)
            self.ie[self.mu_inj] = self.Ie / self.S 

            # coupling constant for resistive coupling between compartments
            self.g_coupling = a/(2*rL*(L/N)**2) 

            # initialize gating variables for each compartment
            self.n = np.ones(self.N) * 0.3177
            self.m = np.ones(self.N) * 0.0529
            self.h = np.ones(self.N) * 0.5961

            self.dv = np.zeros((self.N)) # v[mu,ti] gives voltage at compartment mu and time t

            self.myelinated = myelinated
            if myelinated:
                self.len_myelin = len_myelin
                self.len_ranvier = len_ranvier
                assert self.L/self.N <= len_ranvier, 'Compartement length ({} m) is smaller then length of nodes of Ranvier(2e-6 m).'.format(self.L/self.N)
                if L <= len_myelin: print 'WARNING: The entire axon is myelinated! Increase axon sice if you want nodes of Ranvier!'
                
                num_per_ranvier = np.ceil((len_ranvier/(L/N)))
                num_per_myelin = np.ceil(len_myelin/(L/N))
                num_blocks = np.floor((N - np.floor(0.5*num_per_myelin)) / (num_per_myelin + num_per_ranvier))
                myelin_pos = [True]* int(0.5*num_per_myelin)
                myelin_pos += num_blocks * ([False]*num_per_ranvier + [True]*num_per_myelin)
                myelin_pos += (N-len(myelin_pos)) * [True]
                self.myelin_pos = np.array(myelin_pos, dtype=bool)
                assert N==len(myelin_pos), 'Length of myelin_pos != number of compartments! Fix it!'
                
                print '# ranvier', num_per_ranvier
                print '# myelin', num_per_myelin
                print '# blocks', num_blocks
                print 'size array', len(self.myelin_pos)
            else:
                self.myelin_pos = np.zeros(N)


    def solve_for_time_step(self, ti):
        """
        :param ti: (int) time index
        """

        self.b_prime = np.zeros(self.N)
        self.d_prime = np.zeros(self.N)
        for mu in range(0,self.N):

            if self.myelin_pos[mu]:
                cm = self.cm / 50.
                gL = self.gL / 5000.
                gNa = 0
                gK = 0
            else:
                cm = self.cm
                gL = self.gL
                gNa = self.gNa
                gK = self.gK
            
            g_sum = (gL 
                    + gNa * self.m[mu]**3 * self.h[mu]
                    + gK * self.n[mu]**4)
            g_E_sum = (gL * self.EL 
                        + self.ENa * gNa * self.m[mu]**3 * self.h[mu]
                        + self.EK * gK * self.n[mu]**4)

            # TODO: does updating n,m,h in second loop with new v make a difference?
            # gating variables can be updated after used for the conductances
            v_old = self.v[mu,ti-1]
            dndt = alpha_n(v_old) * (1. - self.n[mu]) - beta_n(v_old) * self.n[mu]
            dmdt = alpha_m(v_old) * (1. - self.m[mu]) - beta_m(v_old) * self.m[mu]
            dhdt = alpha_h(v_old) * (1. - self.h[mu]) - beta_h(v_old) * self.h[mu]
            self.m[mu] = self.m[mu] + dmdt * self.dt 
            self.h[mu] = self.h[mu] + dhdt * self.dt
            self.n[mu] = self.n[mu] + dndt * self.dt

            A = self.g_coupling / cm
            C = self.g_coupling / cm
            D = (self.ie[mu] + g_E_sum) / cm

            a = A * self.z * self.dt
            c = self.g_coupling / self.prev_cm * self.z * self.dt # hacky! using c from previous compartment for calc b'

            # open ends V(mu-1)=V(N+1)=0
            if mu==0:
                B = - (self.g_coupling + g_sum) / cm 
                b = B * self.z * self.dt
                d = (D + B * self.v[mu,ti-1] + C * self.v[mu+1,ti-1]) * self.dt
                self.b_prime[mu] = b
                self.d_prime[mu] = d
            else :
                if mu==self.N-1:
                    B = - (self.g_coupling + g_sum) / cm 
                    b = B * self.z * self.dt
                    d = (D + A * self.v[mu-1,ti-1] + B * self.v[mu,ti-1] ) * self.dt
                else:   
                    B = - (2*self.g_coupling + g_sum) / cm 
                    b = B * self.z * self.dt
                    d = (D + A * self.v[mu-1,ti-1] + B * self.v[mu,ti-1] + C * self.v[mu+1,ti-1]) * self.dt
                self.b_prime[mu] = b + (a * c) / (1 - self.b_prime[mu-1])
                self.d_prime[mu] = d + (a * self.d_prime[mu-1]) / (1 - self.b_prime[mu-1])

            self.prev_cm = cm
            
        self.dv[-1] = self.d_prime[-1] / (1 - self.b_prime[-1])
        for mu in reversed(range(0,len(self.dv)-1)):
            self.dv[mu] = (c * self.dv[mu+1] + self.d_prime[mu]) / (1 - self.b_prime[mu])

        self.v[:,ti] = self.v[:,ti-1] + self.dv


    def solve(self, t, dt, a_array=np.array([])):
        """
        :param t: (scalar) runtime
        :param dt: (scalar) time step
        """
        self.dt = dt
        time_steps = int(float(t)/dt)

        if a_array.size==0:
            self.v = np.zeros((self.N,time_steps)) # v[mu,t] gives voltage at compartment mu and time t
            self.v[:,0] = np.ones(self.N) * self.v0
            self.t = np.zeros(time_steps)
    
            for ti in range(1,time_steps):
                self.solve_for_time_step(ti)
                self.t[ti] = self.t[ti-1] + dt

        else:
            self.vel = np.zeros(a_array.size)
            for i,a in enumerate(a_array):
                self.a = a
                self.v = np.zeros((self.N,time_steps)) # v[mu,t] gives voltage at compartment mu and time t
                self.v[:,0] = np.ones(self.N) * self.v0
                self.t = np.zeros(time_steps)
                self.S = (2 * np.pi * self.a * self.L/self.N)
                self.ie[self.mu_inj] = self.Ie / self.S 

                # coupling constant for resistive coupling between compartments
                self.g_coupling = self.a/(2*self.rL*(self.L/self.N)**2) 

                # initialize gating variables for each compartment
                self.n = np.ones(self.N) * 0.3177
                self.m = np.ones(self.N) * 0.0529
                self.h = np.ones(self.N) * 0.5961

                self.dv = np.zeros((self.N)) # v[mu,ti] gives voltage at compartment mu and time t
    
                for ti in range(1,time_steps):
                    self.solve_for_time_step(ti)
                    self.t[ti] = self.t[ti-1] + dt

                # TODO mu1 mu2 in argument function
                self.calculate_propagation_velocity(100,200)
                self.vel[i] = self.prop_velocity





    def find_peaks(self, t1, t2, thresh=0.):
        t1[t1<thresh] = -1000
        t2[t2<thresh] = -1000
        return np.greater(t1,t2)

    def calculate_propagation_velocity(self, mu1, mu2, test=0):

        #TODO: calc mean and std of different peak differences
        peaks1 = argrelextrema(self.v[mu1,:], self.find_peaks)[0]
        peaks2 = argrelextrema(self.v[mu2,:], self.find_peaks)[0]
        print 'peaks1', peaks1.shape
        print 'peaks2', peaks2.shape
        if peaks1.size<=test or peaks2.size<=test:
            self.prop_velocity = 0
            return 0
        else:
            idx1 = peaks1[test]
            idx2 = peaks2[test]
            delta_x = float((mu2-mu1)) * self.L/float(self.N)
            print 'delta x', delta_x
            delta_t = self.t[idx2] - self.t[idx1]
            print 'delta t', delta_t
            self.prop_velocity = delta_x/delta_t


    def solve_vel_axon(self, a_array):
        for a in a_array:
            self.a = a
            solve



    
