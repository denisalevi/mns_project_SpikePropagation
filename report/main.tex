\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{xcolor}

\usepackage{listings}

\definecolor{Code}{rgb}{0,0,0}
\definecolor{Decorators}{rgb}{0.5,0.5,0.5}
\definecolor{Numbers}{rgb}{0.5,0,0}
\definecolor{MatchingBrackets}{rgb}{0.25,0.5,0.5}
\definecolor{Keywords}{rgb}{0,0,1}
\definecolor{self}{rgb}{0,0,0}
\definecolor{Strings}{rgb}{0,0.63,0}
\definecolor{Comments}{rgb}{0,0.63,1}
\definecolor{Backquotes}{rgb}{0,0,0}
\definecolor{Classname}{rgb}{0,0,0}
\definecolor{FunctionName}{rgb}{0,0,0}
\definecolor{Operators}{rgb}{0,0,0}
\definecolor{Background}{rgb}{0.98,0.98,0.98}

\usepackage{color}
\usepackage{listings}
\usepackage{setspace}

\lstnewenvironment{python}[1][]{
\lstset{
numbers=left,
numberstyle=\footnotesize,
numbersep=1em,
xleftmargin=1em,
framextopmargin=2em,
framexbottommargin=2em,
showspaces=false,
showtabs=false,
showstringspaces=false,
frame=l,
tabsize=4,
% Basic
basicstyle=\ttfamily\small\setstretch{1},
backgroundcolor=\color{Background},
language=Python,
% Comments
commentstyle=\color{Comments}\slshape,
% Strings
stringstyle=\color{Strings},
morecomment=[s][\color{Strings}]{"""}{"""},
morecomment=[s][\color{Strings}]{'''}{'''},
% keywords
morekeywords={import,from,class,def,for,while,if,is,in,elif,else,not,and,or,print,break,continue,return,True,False,None,access,as,,del,except,exec,finally,global,import,lambda,pass,print,raise,try,assert},
keywordstyle={\color{Keywords}\bfseries},
% additional keywords
morekeywords={[2]@invariant},
keywordstyle={[2]\color{Decorators}\slshape},
emph={self},
emphstyle={\color{self}\slshape},
%
}}{}

\title{Models of Neural System \\ Project 3: Action potential propagation}
\author{Julia Jaatela, Florian Stelzer, Denis Alevi}
%\date{January 2016}

\begin{document}

\maketitle

\section{Background}

The aim of this project is to model the propagation of an action potential along an axon. The relationship between the membrane current $i_m$ and the voltage $V$ along an axon is given by the equation
\begin{equation} 
c_m\frac{\partial V}{\partial t } = \frac{1}{2ar_L} \frac{\partial}{\partial x} \Big(a^2 \frac{\partial V}{\partial x}\Big) -i_m +i_e
\end{equation} 

where a is the radius of the axon, $r_L$ is the intracellular resistivity and $c_m$ the specific membrane capacitance. The ionic current $i_m$ flowing through a patch of axonal membrane is well-described by the Hodgkin-Huxley model:

\begin{equation} 
i_m=\bar{g}_L(V-E_L)+\bar{g}_{Na}m^3h(V-E_{Na})+\bar{g}_Kn^4(V-E_K)
\end{equation} 

where m, h and n are Hodgkin-Huxley-type gating variables. Combining the two equations leads to a partial differential equation that can
be solved numerically by a multi-compartmental approximation. In a nonbranching cable, each compartment $\mu$ is coupled to two neighbors, and the equations for the membrane potentials of the compartments are

\begin{equation}  
c_m\frac{dV_{\mu}}{d t } = -i^{\mu}_m + \frac{-i^{\mu}_e}{A_{\mu}} + g_{\mu,\mu+1}(V_{\mu+1}-V_{\mu})+ g_{\mu,\mu-1}(V_{\mu-1}-V_{\mu})
\end{equation} 

where $\mu$ labels the compartments, $i^{\mu}_e$ is the total electrode current flowing into the compartment $\mu$, and $A_{\mu}$ is its surface area. The constant $g_{\mu,\mu-1}$ determines the resistive coupling from neighboring compartment $\mu-1$ to compartment $\mu$ and, for nonbranching cables, can be shown to be equal to $g_{\mu,\mu-1}=a2r_LL^2$ for a length L of each compartment. This defines a system of ordinary differential equations, which can be solved by generalized Euler methods.

\section{Problems}

\subsection*{Problem 1}
Task is, to numerically solve the cable equation (equation 3) when injecting a steady depolarizing current halfway along the cable. We consider that the membrane is passive, i.e. $i_m=(V-E_L)/r_m$, where $r_m$ is the specific membrane resistance. Variables are selected so that
$r_m=20 k\Omega cm^2, r_L =200 \Omega cm, c_m =1 \mu F/cm^2$, and cable radius $a=2 \mu m$. Finally, we compare the solution to the analytical solution of the infinite cable.

\subsubsection*{Analytical solution}
The analytical solution can be calculated by setting $\frac{dV_{\mu}}{d t} = 0$
\[ 
    c_m\frac{\partial V}{\partial t } = \frac{1}{2ar_L} \frac{\partial}{\partial x} \Big(a^2 \frac{\partial V}{\partial x}\Big) -i_m +i_e =  0
\]
We determine membrane time constant $\tau_m = r_mc_m$ and electronic length constant $\lambda=\sqrt{\frac{ar_m}{2r_L}}$. This leads to equation
\[ 
\tau_m\frac{\partial V}{\partial t } = \lambda^2 \frac{\partial ^2V}{\partial x^2} -V +r_mi_e= 0
\]
We also write that current injection $i_e$ is zero everywhere
except within a small region of size $\Delta x$ around the injection site $x=0$. For $|x|>\frac{\Delta x}{2}$ we get
\[ 
\tau_m\frac{\partial V}{\partial t } = \lambda^2 \frac{\partial ^2V}{\partial x^2} -V = 0
\]
\[ V = \lambda^2 \frac{\partial ^2V}{\partial x^2} \]
\[ \Rightarrow V = B_1 \exp(-x/\lambda) + B_2 \exp( x/\lambda) \]
For boundary conditions, we first determine that $\lim_{x \to \pm \infty} = 0$. This results to the conditions $x>0: B_2=0$ and $x<0: B_1=0$. The second boundary condition is that voltage should be symmetric and continuous. These two condition lead to solution $B_1=B_2=B$, and the voltage equation is therefore
\[ V = B \exp(-|x|/\lambda)  \] 
To determine B, we need to look at the voltage in site $\frac{-\Delta x}{2}\leq	x\leq\frac{\Delta x}{2}$. The current per unit area is $i_e = \frac{I_e}{2\pi a\Delta x}$. 
\[ 
\tau_m\frac{\partial V}{\partial t } = \lambda^2 \frac{\partial ^2V}{\partial x^2} -V +r_mi_e = 0\]
\[ v - r_mi_e =  \lambda^2 \frac{\partial ^2V}{\partial x^2}\]
\[ v - r_m\frac{I_e}{2\pi a \Delta x} =  \lambda^2 \frac{1}{\Delta x} \Big(\frac{\partial V}{\partial x}\bigg|_{\Delta x / 2}
-\frac{\partial V}{\partial x}\bigg|_{-\Delta x / 2}\Big)\]
\[v\Delta x - \frac{r_m I_e}{2\pi a} =  \lambda^2 \Big(\frac{-B}{\lambda} \exp(\frac{-\Delta x}{2\lambda})
-\frac{B}{\lambda} \exp(\frac{-\Delta x}{2\lambda})\Big)\]
For $\Delta x \rightarrow 0$, the exponential terms go to one and we get
\[\frac{r_m I_e}{2\pi a} =  -2 B \lambda \]
\[B = \frac{I_e}{2} \underbrace{\frac{r_m}{2\pi a \lambda}}_{R_{\lambda}} =  \frac{I_e R_{\lambda}}{2},\]
where $R_{\lambda}$ is the input resistance. Analytical solution for steady state voltage in the infinite cable is therefore 
\[V(x) = \frac{I_e R_{\lambda}}{2}\exp(\frac{-|x|}{\lambda})\]

\subsubsection*{Numerical solution}
We are supposed to solve the steady cable equation with an injected current at the center of the cable. In order to get close to the case of an infinite cable we use the sealed end condition. That is in our case V'(-5) = 0.

First we define the steady state cable equation $\frac{\partial V}{\partial t} = 0$. However, consider an injected current on an 0.2 mm interval around the point 0 instead of current that is precisely injected at 0. A smaller interval would lead to unwanted numerical effects, no matter how small we choose the spatial grid. Steady state cable equation (1) is divided to two ODE's that are then solved with the odeint() function from the SciPy package. 
\begin{align*}
\frac{dy}{dx} &= \frac{dV}{dx} \\
\frac{dy}{dx} &= \frac{2r_L}{a}\frac{V - E_L}{r_m}-i_e \\
\end{align*}

\begin{figure}
    \centering
    \subfloat[$V(-5\,\text{mm})= 0.0107\,\text{V}$]{{\includegraphics[width=5.5cm]{ex1_small_current} }}
    \qquad
    \subfloat[$V(-5\,\text{mm})= 0.0108\,\text{V}$]{{\includegraphics[width=5.5cm]{ex1_large_current} }}
    \caption{Picture a shows the numerical solution for an initial value that is too small. Picture b show the solution for an initial value that is too large.}
    \label{fig:initial values}
\end{figure}

We consider a cable of 1 cm length and then choose the initial value $V(-5\,\text{mm}) = 0.010700\,\text{V}$ we get a curve as shown in Figure \ref{fig:initial values}a. As we can see, the sealed end condition is not satifsied on the right side of the plot.
The slope at $x=5$ mm is negative.

If we try a slightly larger initial value $V(-5\,\text{mm}) = 0.010800\,\text{V}$, we can see that the sealed end condition is not satisfied again and the slope at $x=5$ mm is positive (Figure \ref{fig:initial values}b). 


In order to find the correct initial value, satisfying the sealed end condition at $x=5$ mm, we used the shooting method. In this method we tried different initial values that we then increase and decrease until the sealed end condition was satisfied. The result is an initial value of $V(-5\,\text{mm})=0.010742\,\text{mV}$ and  is shown in Figure \ref{fig:symmetric}.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{ex1_symmetric}
    \caption{Numerical solution with best initial condition that was computed with shooting method.}
    \label{fig:symmetric}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{ex1_comparison}
    \caption{Figure shows the analytical and numerical solutions for steady state cable.}
    \label{fig:comparison}
\end{figure}

When we compare our numerical solution to the analytical one (Figure \ref{fig:comparison}), we observe that our numerical solution is a bit inaccurate around zero and near the ends of the cable. However this is not a big surprise. After all, we simulated a 1 cm long cable with an current injected by a 0.2 mm thick electrode, but we calculated the analytical solution for an infinite cable and a current injected exactly at one point.
In total, we conclude that our numerical solution is accurate enough.

\subsection*{Problem 2}
In this exercise, we implemented the Hodgkin-Huxley model of action potential propagation in the squid giant axon by solving the partial differential equation using the Crank- Nicholson method (see e.g. Chapters 5.5, 5.6 and 6.6B from Dayan and Abbott, 2001). Parameter values were chosen so that $a=238 \mu m$ and $ r_L =35.4 \Omega cm$. 

Equation 3 for the membrane potential within compartment $\mu$ can be written in the form 

\begin{equation}
    \frac{dV_{\mu}}{dt}= A_{\mu}V_{\mu-1}+B_{\mu}V_{\mu}+C_{\mu}V_{\mu+1}+D_{\mu}
    \label{eq:v}
\end{equation}
where
\begin{align*}
 A_{\mu}&=c_m^{-1}g_{\mu,\mu-1}   &  B_{\mu}&=-c_m^{-1}(\sum_i g_i^{\mu}+g_{\mu,\mu+1}+ g_{\mu,\mu-1}) \\
 C_{\mu}&=c_m^{-1}g_{\mu,\mu+1}   &  D_{\mu}&=-c_m^{-1}(\sum_i g_i^{\mu}E_i+I_e^{\mu}/A_{\mu})
\end{align*}
Defining 
\[V_{\mu}(t+\Delta t) = V_{\mu}(t) + \Delta V_{\mu}\]
we can write equation \ref{eq:v} as
\begin{equation}
    \Delta V_{\mu} = (A_{\mu}V_{\mu-1}(t)+B_{\mu}V_{\mu}(t)+C_{\mu}V_{\mu+1}(t)+D_{\mu}) \Delta t
\end{equation}
which is how $\Delta V_{\mu}$ is computed using the Euler method. To increase the stability of the numerical solution, one can evaluate the voltages at later times $t+ z\Delta t$ instead of $t$.
This results in 
\begin{equation}
    \Delta V_{\mu} = (A_{\mu}V_{\mu-1}(t+z\Delta t)+B_{\mu}V_{\mu}(t+z \Delta t)+C_{\mu}V_{\mu+1}(t+z\Delta t)+D_{\mu}) \Delta t.
\end{equation}
Approximating
\[ V_{\mu} (t + z\Delta t) \approx V_{\mu}(t) + z \Delta V_{\mu} \]
one gets a system of coupled linear equations:
\begin{equation}
    \Delta V_{\mu} = a_{\mu} \Delta V_{\mu-1} + b_{\mu} \Delta V_{\mu} + c_{\mu} \Delta V_{\mu +1} + d_{\mu}
    \label{eq:matrix}
\end{equation}
with
\begin{align*}
    a_{\mu} = A_{\mu} z \Delta t, \qquad b_{\mu} =  B_{\mu} z \Delta t, \qquad c_{\mu} = C_{\mu} z \Delta t \\
    d_{\mu} = (D_{\mu} + A_{\mu} V_{\mu-1}(t) + B_{\mu} V_{\mu}(t) + C_{\mu} V_{\mu+1}(t)) \Delta t
\end{align*}
The system of equations \ref{eq:matrix} can be written as a banded diagonal matrix.

\begin{equation}
    \begin{pmatrix}
        b_1 - 1 & c_1   & 0     & 0     & 0      & \cdots & 0 \\
        a_2     & b_2-1 & c_2   & 0     & 0      & \cdots & 0 \\
        0       & a_3   & b_3-1 & c_3   & 0      & \cdots & 0 \\
        \vdots  &\ddots &\ddots &\ddots &\ddots  & \ddots & \vdots \\
        0       &\cdots & 0     &a_{N-2}&b_{N-2}-1&c_{N-2}& 0 \\
        0       &\cdots & 0     & 0     &a_{N-1}& b_{N-1}-1 & c_{N-1} \\
        0       &\cdots & 0     & 0     & 0     & a_{N} & b_N -1  \\
        
    \end{pmatrix}
    \begin{pmatrix}
        \Delta V_1 \\ \Delta V_2 \\ \Delta V_3 \\ \vdots \\ \Delta V_{N-2} \\ \Delta V_{N-1} \\ \Delta V_N
    \end{pmatrix} =
    \begin{pmatrix}
        d_1 \\ d_2 \\ d_3 \\ \vdots \\ d_{N-2} \\ d_{N-1} \\ d_N
    \end{pmatrix}
\end{equation}

There are different methods to solve this banded diagonal matrix, we used the method described in Dayan and Abbott chapter 6.6.

The Crank-Nicholson method, which we used in our simulations, uses $z=0.5$. Another method, the Reverse Euler method, uses $z=1$.

%We define update rule using Crank-Nicholson method with $z=0.5$ and get
%\[V_{\mu}(t + z\Delta t) = V_{\mu}(t)+ z\Delta V_{\mu}\textnormal{, where}\]
%\[\Delta V_{\mu}= a_{\mu}V_{\mu-1}+b_{\mu}V_{\mu}+c_{\mu}V_{\mu+1}+d_{\mu} \textnormal{ and}\]
%\[ a_{\mu}=A_{\mu}z\Delta t, b_{\mu}=B_{\mu}z\Delta t, c_{\mu}=C_{\mu}z\Delta t\]
%\[d_{\mu}=(A_{\mu}V_{\mu-1}(t)+B_{\mu}V_{\mu}(t)+C_{\mu}V_{\mu+1}(t)+D_{\mu})\Delta t.\]
%Using these formulas, update procedure provided in Dayan and Abbot, and Hodgkin-Huxley model, we are able to simulate axon potential propagation. Code for this simulation is provided in Appendix B. 

\subsection*{Problem 3}
Next, we initiated an action potential on one end of the axon by injecting a current in the terminal compartment $\mu=0$. Total cable length was chosen to be $L=1$cm and the cable was divided to $N=200$ compartments. Compartment length was therefore $L/N=0.01\text{m}/200= 50\mu\text{m}$.

Figure \ref{fig:ex3} shows the action potential progression through the compartments. We can see that it takes approximately 4ms for one action potential to go through one compartment. Figure \ref{fig:ex3} corresponds well with the figure 6.17 presented in Theoretical Neuroscience by Dayan and Abbot. 

\begin{figure}[h]
    \centering
    \makebox[\textwidth][c]{\includegraphics[width=1.5\textwidth]{ex3}}
    \caption{The first figure shows the action potential at one time step. The second to figures show how the action potential moves through an individual compartment with respect to time.}
    \label{fig:ex3}
\end{figure}

\subsection*{Problem 4}
In this section we determined the action potential propagation velocity as a function of the axon radius. Velocity is defined by computing the time $t$ when the maximum of the potential occurs at a given spatial location. We then compare $t$ for two different compartments and compute the velocity by dividing the spatial difference by the time difference. 
\[velocity=\frac{\Delta x}{\Delta t} = \frac{x_2 - x_1}{t_2 - t_1}\]
From analytical solution, we know that action potential velocity is proportional to the ratio 
\[\frac{\lambda}{\tau_m} = \sqrt{\frac{a}{c^2_m r_L r_m}},\]
where  $\lambda$ is the electronic length constant and $\tau_m$ is the membrane time constant. This means that the velocity should be proportional to the square root of the axon radius.

Figure \ref{fig:ex4} shows the numerical solution and the corresponding analytical line. We can qualitatively reproduce the analytical dependence. It even seems like the proportionality factor equals $\pi$. The deviation can be due to numerical issues. We can also conclude that we need thick axons if we want 
to achieve high action potential propagation speeds. An example of a extremely thick axon is the squid giant axon used in previous problems, with a radius $a=238 \mu m$. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{ex4}
    \caption{Numerical and analytical solutions for the action potential propagation speeds as a function of axon radius $a$.}
    \label{fig:ex4}
\end{figure}

\subsection*{Problem 5}
Next, we initiated action potentials at both ends of the axon (Figure \ref{fig:ex5_axon}). Task was to show that they annihilate when they collide. This is because of the refractory period of both potentials: action potentials are unable to get past the other one, since potassium channels are still open.

Figure \ref{fig:ex5} shows the simulation. Two action potentials are created at both ends of the axon and they move towards each other. When they meet, the potential at the middle of the action drops, and both potentials disappear.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{ex5_axon}
    \caption{Initiating action potentials at both ends of the axon.}
    \label{fig:ex5_axon}
\end{figure}

\begin{figure}[h]
    \centering
    \makebox[\textwidth][c]{\includegraphics[width=1.5\textwidth]{ex5}}
    \caption{Two action potentials move towards each other and annihilate when colliding.}
    \label{fig:ex5}
\end{figure}

\subsection*{Problem 6}
In this last section, we simulated action potential propagation in a myelinated axon (see e.g. Chapter 6.4 from Dayan and Abbott, 2001). The nodes of Ranvier (the unmyelinated spaces) are considered to be $2 \mu m$ long and are located at $1 mm$ intervals along the axon. Myelin was considered to increases the resistance across the cell membrane by a factor of 5,000 and decreases the capacitance by a factor of 50.

The following graphic from J. E. Dowling (Neurons and Networks, 1992) illustrates how the propagation of action potentials works in myelinated axons.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{dowling416}
    \caption{Action potential propagation along a myelinated axon.}
    \label{fig:ex6}
\end{figure}

Only in the nodes of Ranvier we can observe ion channel activity. However the membrane capacitance is very low in the myelinated parts of the axon. This leads to the following effect: When an action potential ermerges in a node, a potential pulse spreads along the axon. Due to the low capacitance it can move quite far and quite quickly. So it reaches the next node after a short time and is still strong enough to exceed the threshold for the generation of a new action potential. Therefore the action potential regenerates in each node.

Unfortunately we were not able to reproduce this kind of behavior. We guess that our method is not sufficiently numerically stable.

\section*{Appendix: Python code}

\begin{python}

import numpy as np
from scipy.signal import argrelextrema

# Hodgkin-Huxley ion channel dynamics:
# We are using SI units, therefore the equations have been 
# adapted to use v[V] as argument and return rate[1/s]

alpha_n = lambda v: ( 0.01*(v*1e3+55.)/(1-np.exp(-0.1*(v*1e3+55.))) ) * 1e3
beta_n = lambda v: ( 0.125*np.exp(-0.0125*(v*1e3+65.)) ) * 1e3

alpha_m = lambda v: ( 0.1*(v*1e3+40.)/(1-np.exp(-0.1*(v*1e3+40.))) ) * 1e3
beta_m = lambda v: ( 4*np.exp(-0.0556*(v*1e3+65.)) ) * 1e3

alpha_h = lambda v: ( 0.07*np.exp(-0.05*(v*1e3+65.)) ) * 1e3
beta_h = lambda v: ( 1/(1+np.exp(-0.1*(v*1e3+35.))) ) * 1e3
    


class MultiCompartmentHodgkinHuxley(object):
    """ Class to model an axon with a multi-compartment Hodgkin-Huxley model. 
        All compartments are assumes to have same size."""

    def __init__(self, N, a=238e-6, L=1e-2, rL=35.4e-2, cm=1e-2, 
            mu_inj=[1], Ie=[10e-9], v0=-65e-3, gL=0.3, gK=3.6e2, 
            gNa=1.2e3, EL=-54.387e-3, EK=-77e-3, ENa=50e-3, z=0.5,
            myelinated=False, len_ranvier=2e-6, len_myelin=1e-3):

            """
            :param N: (int) number of compartments
            :param a: (scalar, default=238e-6) cable radius [m]
            :param L: (scalar, default=1e-2) total cable length [m]
            :param rL: (scalar, default=35.4e-2) intracellular 
                        resistivity [Ohm*m]
            :param cm: (scalar, default=1e-2) specific membrane 
                        capacitance [F/(m^2)]
            :param mu_inj: (array of int, default=[0]) compartment 
                        of current injection(s)
            :param Ie: (array of scalars, default=[10e-9]) total 
                        external current injected in compartments 
                        specified in mu_inj, len(mu_inj) must equal len(Ie) [A]
            :param v0: (scalar, default=-65e-3) initial membrane voltage [V]
            :param gL: (scalar, default=0.3) leak conductance [S/((m^2)]
            :param gK: (scalar, default=3.6e2) maximum conductance of 
                        potassium channel [S/(m^2)]
            :param gNa: (scalar, default=1.2e3) maximum conductance of 
                        sodium channel [S/(m^2)]
            :param EL: (scalar, default=-54.387e3) reversal potential of 
                        leakage [V]
            :param EK: (scalar, default=-77e-3) reversal potential of 
                        potassium channel [V]
            :param ENa: (scalar, default=50e-3) reversal potential of 
                        sodium channel [V]
            :param z: (scalar, default=0.5) z=0.5 is Crank-Nicholson, 
                        z=1 is reverse Euler method
            :param meyelinated: (bool, default=False) if True, the axon 
                        is split in blocks of myelinated compartement 
                        and rode of ranvier nodes
            :param len_ranvier: (scalar, default=2e-6) if myelinated==True: 
                        length of nodes of ranvier [m]
            :param len_myelin: (scalar, default=1e-3) if myelinated==True: 
                        length of myelinated parts [m]

            """

            assert len(mu_inj)==len(Ie), ("Number of injection sides len(mu_inj) 
                        and number of current values len(Ie) are not equal!")

            self.a = a
            self.L = L
            self.rL = rL
            self.cm = cm
            self.Ie = np.array(Ie)
            self.mu_inj = np.array(mu_inj)
            self.N = N
            self.v0 = v0
            self.gL = gL
            self.gK = gK
            self.gNa = gNa
            self.EL = EL
            self.EK = EK
            self.ENa = ENa
            self.z = z
            self.myelinated = myelinated

            self.prev_cm = cm # hacky! for myelinated C and c calculation 

            # electronic length constant lambda
            self.l = np.sqrt( a / (2*rL* ( gL
                        / (2 * np.pi * self.a * (self.L/self.N) * 1e-4)))) 
            # analytical spike propagation velocity
            self.anal_vel = np.sqrt(a/(2*cm**2*rL*(1./gL)))

            # suface area of compartments
            self.S = (2 * np.pi * self.a * self.L/self.N)
            # injected current per unit are [A/(m^2)], 
            # value given by scaled Ie for given injection positions, 0 else
            self.ie = np.zeros(self.N)
            self.ie[self.mu_inj] = self.Ie / self.S 

            # coupling constant for resistive coupling between compartments
            self.g_coupling = a/(2*rL*(L/N)**2) 

            # initialize gating variables for each compartment
            self.n = np.ones(self.N) * 0.3177
            self.m = np.ones(self.N) * 0.0529
            self.h = np.ones(self.N) * 0.5961
            
            # initialize delta V array
            self.dv = np.zeros((self.N)) 

            if myelinated:
                self.len_myelin = len_myelin
                self.len_ranvier = len_ranvier

                assert self.L/self.N <= len_ranvier, ('Compartement 
                    length ({} m) is smaller then length of nodes of 
                    Ranvier(2e-6 m).'.format(self.L/self.N))

                if L <= len_myelin: print ('WARNING: The entire axon 
                    is myelinated! Increase axon sice if you want 
                    nodes of Ranvier!')
                
                # little complicated way of creating an boolean array 
                # specifying for each compartment if its myelinated [True] 
                # or not [False] number of compartments per myelinated or 
                # ranvier part and per block (one ranvier + one ranvier part)
                num_per_ranvier = np.ceil((len_ranvier/(L/N)))
                num_per_myelin = np.ceil(len_myelin/(L/N))
                num_blocks = (np.floor((N - np.floor(0.5*num_per_myelin)) 
                                / (num_per_myelin + num_per_ranvier)))

                # start with myelinated compartments of size 0.5*len_myelin
                myelin_pos = [True]* int(0.5*num_per_myelin)
                # add as many nodes of ranvier and myelinated parts as fit 
                # in axon length
                myelin_pos += num_blocks * ([False]*num_per_ranvier 
                                            + [True]*num_per_myelin)
                # fill up the left compartments as myelinated
                myelin_pos += (N-len(myelin_pos)) * [True]

                self.myelin_pos = np.array(myelin_pos, dtype=bool)
                assert N==len(myelin_pos), ('Length of myelin_pos != 
                                    number of compartments! Fix it!')
                
                print '# ranvier', num_per_ranvier
                print '# myelin', num_per_myelin
                print '# blocks', num_blocks
                print 'size array', len(self.myelin_pos)
            else: # all compartments not myelinated
                self.myelin_pos = np.zeros(N)


    def solve_for_time_step(self, ti):
        """
        Solve the coupled set of linear differential equations 
        (all compartments) for the membrane voltage for a single 
        timestep. The steps described in Dayan and Abbott chapter 6.6B 
        are implemented here with simplifications for same sized 
        compartments.

        :param ti: (int) time index
        """

        self.b_prime = np.zeros(self.N)
        self.d_prime = np.zeros(self.N)

        # loop through all compartments to get d_prine and b_prime values
        for mu in range(0,self.N):

            if self.myelin_pos[mu]:
                # muelin decreases membrane capacitance and leak conductance
                cm = self.cm / 50.
                gL = self.gL / 5000.
                # no ion channels can be used when myelin is present
                gNa = 0
                gK = 0
            else:
                cm = self.cm
                gL = self.gL
                gNa = self.gNa
                gK = self.gK
            
            g_sum = (gL 
                    + gNa * self.m[mu]**3 * self.h[mu]
                    + gK * self.n[mu]**4)
            g_E_sum = (gL * self.EL 
                        + self.ENa * gNa * self.m[mu]**3 * self.h[mu]
                        + self.EK * gK * self.n[mu]**4)

            # update channel variables
            v_old = self.v[mu,ti-1]
            dndt = alpha_n(v_old) * (1. - self.n[mu]) - beta_n(v_old) * self.n[mu]
            dmdt = alpha_m(v_old) * (1. - self.m[mu]) - beta_m(v_old) * self.m[mu]
            dhdt = alpha_h(v_old) * (1. - self.h[mu]) - beta_h(v_old) * self.h[mu]
            self.m[mu] = self.m[mu] + dmdt * self.dt 
            self.h[mu] = self.h[mu] + dhdt * self.dt
            self.n[mu] = self.n[mu] + dndt * self.dt

            A = self.g_coupling / cm
            C = self.g_coupling / cm
            D = (self.ie[mu] + g_E_sum) / cm

            a = A * self.z * self.dt
            c = self.g_coupling / self.cm * self.z * self.dt
            # hacky! using c from previous compartment for calc b_prime
            c_prev = self.g_coupling / self.prev_cm * self.z * self.dt             

            # first compartment a=0, g_coupling(-1,0)=0 and b'=b, d'=d
            if mu==0: 
                B = - (self.g_coupling + g_sum) / cm 
                b = B * self.z * self.dt
                d = (D + B * self.v[mu,ti-1] + C * self.v[mu+1,ti-1]) * self.dt
                self.b_prime[mu] = b
                self.d_prime[mu] = d
            else :
                if mu==self.N-1: # last compartment c=0, g_coupling(N,N+1)=0
                    B = - (self.g_coupling + g_sum) / cm 
                    b = B * self.z * self.dt
                    d = ((D + A * self.v[mu-1,ti-1] + B * self.v[mu,ti-1] ) 
                            * self.dt)
                else:   
                    B = - (2*self.g_coupling + g_sum) / cm 
                    b = B * self.z * self.dt
                    d = (D + A * self.v[mu-1,ti-1] + B * self.v[mu,ti-1] 
                        + C * self.v[mu+1,ti-1]) * self.dt
                self.b_prime[mu] = b + (a * c) / (1 - self.b_prime[mu-1])
                self.d_prime[mu] = (d + (a * self.d_prime[mu-1]) 
                                    / (1 - self.b_prime[mu-1]))
            # save cm for case of change btwn myelinated and not myelinated 
            # compartment for calc c_(mu-1) 
            self.prev_cm = cm 
            
        # loop through the compartments in reversed order to get the 
        # Delta v values
        self.dv[-1] = self.d_prime[-1] / (1 - self.b_prime[-1])
        for mu in reversed(range(0,len(self.dv)-1)):
            self.dv[mu] = ((c * self.dv[mu+1] + self.d_prime[mu]) 
                            / (1 - self.b_prime[mu]))

        # update voltage of all compartments for given time step
        self.v[:,ti] = self.v[:,ti-1] + self.dv


    def solve(self, t, dt, a_array=np.array([])):
        """
        Solve Multi-Compartment Hodgkin-Huxley neuron for a 
        runtime t with time step dt.

        :param t: (scalar) runtime
        :param dt: (scalar) time step
        :param a_array: (np.array, default empty) Values of axon radiuses a. 
                For each a in a_array the model is solved and the propagation 
                velocity is saved
        """
        self.dt = dt
        time_steps = int(float(t)/dt)

        if a_array.size==0:
            # v[mu,t] gives voltage at compartment mu and time t
            self.v = np.zeros((self.N,time_steps)) 
            self.v[:,0] = np.ones(self.N) * self.v0
            self.t = np.zeros(time_steps)
    
            for ti in range(1,time_steps):
                self.solve_for_time_step(ti)
                self.t[ti] = self.t[ti-1] + dt

        # the following was implemented but not used for plot generation 
        # in report! We used a outside the classe.
        else:
            self.vel = np.zeros(a_array.size)
            for i,a in enumerate(a_array):
                # reset initial values and arrays
                self.a = a
                self.v = np.zeros((self.N,time_steps))
                self.v[:,0] = np.ones(self.N) * self.v0
                self.t = np.zeros(time_steps)
                self.S = (2 * np.pi * self.a * self.L/self.N)
                self.ie[self.mu_inj] = self.Ie / self.S 

                # coupling constant for resistive coupling between compartments
                self.g_coupling = self.a/(2*self.rL*(self.L/self.N)**2) 

                # initialize gating variables for each compartment
                self.n = np.ones(self.N) * 0.3177
                self.m = np.ones(self.N) * 0.0529
                self.h = np.ones(self.N) * 0.5961

                self.dv = np.zeros((self.N)) 
    
                for ti in range(1,time_steps):
                    self.solve_for_time_step(ti)
                    self.t[ti] = self.t[ti-1] + dt

                # TODO mu1 mu2 in argument function
                self.calculate_propagation_velocity(100,200)
                self.vel[i] = self.prop_velocity


    def find_peaks(self, mu1, mu2, thresh=0.):
        """ Find greater value between two inputs mu1, mu2. If inputs are below 
            the threshold thresh, set them to very low value (-1000)
        """
        mu1[mu1<thresh] = -1000
        mu2[mu2<thresh] = -1000
        return np.greater(mu1,mu2)


    def calculate_propagation_velocity(self, mu1, mu2, n=0):
        """
        Calculate propagation velosity between the nth spikes of 
        compartment mu1 and mu2.
        """

        # get array of all local extrema using self.find_peaks function
        peaks1 = argrelextrema(self.v[mu1,:], self.find_peaks)[0]
        peaks2 = argrelextrema(self.v[mu2,:], self.find_peaks)[0]
        print 'peaks1', peaks1.shape
        print 'peaks2', peaks2.shape
        # in case of too less spiking, set velocity to 0
        if peaks1.size<=n or peaks2.size<=n:
            self.prop_velocity = 0
            return 0
        else:
            # get indices of peak positions
            idx1 = peaks1[n]
            idx2 = peaks2[n]
            delta_x = float((mu2-mu1)) * self.L/float(self.N)
            print 'delta x', delta_x
            delta_t = self.t[idx2] - self.t[idx1]
            print 'delta t', delta_t
            self.prop_velocity = delta_x/delta_t
\end{python}


\end{document}
