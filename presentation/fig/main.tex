\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{subfig}
\title{Models of Neural System \\ Project 3: Action potential propagation}
\author{ }
\date{January 2016}

\begin{document}

\maketitle

\section{Background}

The aim of this project is to model the propagation of an action potential along an axon. The relationship between the membrane current $i_m$ and the voltage $V$ along an axon is given by the equation
\begin{equation} 
c_m\frac{\partial V}{\partial t } = \frac{1}{2ar_L} \frac{\partial}{\partial x} \Big(a^2 \frac{\partial V}{\partial x}\Big) -i_m +i_e
\end{equation} 

where a is the radius of the axon, $r_L$ is the intracellular resistivity and $c_m$ the specific membrane capacitance. The ionic current $i_m$ fowing through a patch of axonal membrane is well-described by the Hodgkin-Huxley model:

\begin{equation} 
i_m=\bar{g}_L(V-E_L)+\bar{g}_{Na}m^3h(V-E_{Na})+\bar{g}_Kn^4(V-E_K)
\end{equation} 

where m, h and n are Hodgkin-Huxley-type gating variables. Combining the two equations leads to a partial differential equation that can
be solved numerically by a multi-compartmental approximation. In a nonbranching cable, each compartment $\mu$ is coupled to two neighbors, and the equations for the membrane potentials of the compartments are

\begin{equation}  
c_m\frac{dV_{\mu}}{d t } = -i^{\mu}_m + \frac{-i^{\mu}_e}{A_{\mu}} + g_{\mu,\mu+1}(V_{\mu+1}-V_{\mu})+ g_{\mu,\mu-1}(V_{\mu-1}-V_{\mu})
\end{equation} 

where $\mu$ labels the compartments, $i^{\mu}_e$ is the total electrode current flowing into the compartment $\mu$, and $A_{\mu}$ is its surface area. The constant $g_{\mu,\mu-1}$ determines the resistive coupling from neighboring compartment $\mu-1$ to compartment $\mu$ and, for nonbranching cables, can be shown to be equal to $g_{\mu,\mu-1}=a2r_LL^2$ for a length L of each compartment. This defines a system of ordinary differential equations, which can be solved by generalized Euler methods.

\section{Problems}

\subsection*{Problem 1}
Task is, to numerically solve the cable equation (equation 3) when injecting a steady depolarizing current halfway along the cable. We consider that the membrane is passive, i.e. $i_m=(V-E_L)/r_m$, where $r_m$ is the specific membrane resistance. Variables are selected so that
$r_m=20 k\Omega cm^2, r_L =200 \Omega cm, c_m =1 \mu F/cm^2$, and cable radius $a=2 \mu m$. Finally, we compare the solution to the analytical solution of the infinite cable.

\subsubsection*{Analytical solution}
The analytical solution can be calculated by setting $\frac{dV_{\mu}}{d t} = 0$
\[ 
c_m\frac{\partial V}{\partial t } = \frac{1}{2ar_L} \frac{\partial}{\partial x} \Big(a^2 \frac{\partial V}{\partial x} -i_m +i_e\Big)= 0
\]
We determine membrane time constant $\tau_m = r_mc_m$ and electronic length constant $\lambda=\sqrt{\frac{ar_m}{2r_L}}$. This leads to equation
\[ 
\tau_m\frac{\partial V}{\partial t } = \lambda^2 \frac{\partial ^2V}{\partial x^2} -V +r_mi_e= 0
\]
We also write that current injection $i_e$ is zero everywhere
except within a small region of size $\Delta x$ around the injection site $x=0$. For $|x|>\frac{\Delta x}{2} we get$
\[ 
\tau_m\frac{\partial V}{\partial t } = \lambda^2 \frac{\partial ^2V}{\partial x^2} -V = 0
\]
\[ V = \lambda^2 \frac{\partial ^2V}{\partial x^2} \]
\[ \Rightarrow V = B_1 exp(-x/\lambda) + B_2 exp( x/\lambda) \]
For boundary conditions, we first determine that $\lim_{x \to \pm \infty} = 0$. This results to the conditions $x>0: B_2=0$ and $x<0: B_1=0$. The second boundary condition is that voltage should be symmetric and continuous. These two condition lead to solution $B_1=B_2=B$, and the voltage equation is therefore
\[ V = B exp(-|x|/\lambda)  \] 
To determine B, we need to look at the voltage in site $\frac{-\Delta x}{2}\leq	x\leq\frac{\Delta x}{2}$. The current is now $i_e = \frac{I_e}{2\pi a\Delta x}$. 
\[ 
\tau_m\frac{\partial V}{\partial t } = \lambda^2 \frac{\partial ^2V}{\partial x^2} -V +r_mi_e = 0\]
\[ v - r_mi_e =  \lambda^2 \frac{\partial ^2V}{\partial x^2}\]
\[ v - r_m\frac{I_e}{2\pi a \Delta x} =  \lambda^2 \frac{1}{\Delta x} \Big(\frac{\partial V}{\partial x}\bigg|_{\Delta x / 2}
-\frac{\partial V}{\partial x}\bigg|_{-\Delta x / 2}\Big)\]
\[v\Delta x - \frac{r_m I_e}{2\pi a} =  \lambda^2 \Big(\frac{-B}{\lambda} exp(\frac{-\Delta x}{2\lambda})
-\frac{B}{\lambda} exp(\frac{-\Delta x}{2\lambda})\Big)\]
For $\Delta x \rightarrow 0$, the exponential terms go to one and we get
\[\frac{r_m I_e}{2\pi a} =  -2 B \lambda \]
\[B = \frac{I_e}{2} \underbrace{\frac{r_m}{2\pi a \lambda}}_{R_{\lambda}} = B = \frac{I_e R_{\lambda}}{2},\]
where $R_{\lambda}$ is the input resistance. Analytical solution for steady state voltage in the infinite cable is therefore 
\[V(x) = \frac{I_e R_{\lambda}}{2}exp(\frac{-|x|}{\lambda})\]

\subsubsection*{Numerical solution}
We are supposed to solve the steady cable equation with an injected current at the center of the cable. In order to get close to the case of an infinite cable we use the sealed end condition. That is in our case V'(-5) = 0.

First we define the steady state cable equation $\frac{\partial V}{\partial t} = 0$. However, consider an injected current on an 0.2 mm interval around the point 0 instead of current that is precisely injected at 0. A smaller interval would lead to unwanted numerical effects, no matter how small we choose the spatial grid. Steady state cable equation (1) is divided to two ODE's that are then solved with Python method odeint(). 
\begin{align*}
\frac{dy}{dx} &= \frac{dV}{dx} \\
\frac{dy}{dx} &= \frac{2r_L}{a}\frac{V - E_L}{r_m}-i_e \\
\end{align*}

\begin{figure}
    \centering
    \subfloat[V(-5)= 0.0107]{{\includegraphics[width=5.5cm]{ex1_small_current} }}
    \qquad
    \subfloat[V(-5)= 0.0108]{{\includegraphics[width=5.5cm]{ex1_large_current} }}
    \caption{Picture a shows the analytical solution for an initial value that is too small. Picture b show the solution for an initial value that is too large.}
    \label{fig:initial values}
\end{figure}

We consider a cable of 1 cm length and then choose the initial value $V(-5) = 0.0107$ we get a curve as shown in Figure \ref{fig:initial values}a. As we can see, the plot is not symmetric. However, symmetry is sensible assumption for our problem. The voltage on both sides of the injection site should reveal an coinciding behavior. 

If we try a slightly larger initial value V(-5) = 0.0108, we can see that the solution still isn't symmetric (Figure \ref{fig:initial values}b). 


In order to find the correct initial value, we used the shooting method. In this method we tried different initial values that we then increase and decrease until we obtain an sufficiently symmetric voltage curve. The result is shown in Figure \ref{fig:symmetric}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{ex1_symmetric}
    \caption{Symmetric numerical solution that was computed with shooting method.}
    \label{fig:symmetric}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{ex1_comparison}
    \caption{Figure shows the analytical and numerical solutions for steady state cable.}
    \label{fig:comparison}
\end{figure}

When we compare our numerical solution to the analytical one (Figure \ref{fig:comparison}), we observe that our numerical solution is a bit inaccurate around zero and near the ends of the cable. However this is not a big surprise. After all, we simulated a 1 cm long cable with an current injected by a 0.2 mm thick electrode, but we calculated the analytical solution for an infinite cable and a current injected exactly at one point.
In total, we conclude that our numerical solution is accurate enough.

\subsection*{Problem 2}
In this exercise, we implemented the Hodgkin-Huxley model of action potential propagation in the squid giant axon by solving the partial differential equation using the Crank- Nicholson method (see e.g. Chapters 5.5, 5.6 and 6.6B from Dayan and Abbott, 2001). Parameter values were chosen so that $a=238 \mu m$ and $ r_L =35.4 \Omega cm$. 

Equation 3 for the membrane potential within compartment $\mu$ can be written in the form 

\[\frac{dV_{\mu}}{dt}= A_{\mu}V_{\mu-1}+B_{\mu}V_{\mu}+C_{\mu}V_{\mu+1}+D_{\mu}\textnormal{, where}
\]
\begin{align*}
 A_{\mu}&=c_m^{-1}g_{\mu,\mu-1}   &  B_{\mu}&=-c_m^{-1}(\sum_i g_i^{\mu}+g_{\mu,\mu+1}+ g_{\mu,\mu-1}) \\
 C_{\mu}&=c_m^{-1}g_{\mu,\mu-1}   &  D_{\mu}&=-c_m^{-1}(\sum_i g_i^{\mu}E_i+I_e^{\mu}/A_{\mu})
\end{align*}
We define update rule using Crank-Nicholson method with $z=0.5$ and get
\[V_{\mu}(t + z\Delta t) = V_{\mu}(t)+ z\Delta V_{\mu}\textnormal{, where}\]
\[\Delta V_{\mu}= a_{\mu}V_{\mu-1}+b_{\mu}V_{\mu}+c_{\mu}V_{\mu+1}+d_{\mu} \textnormal{ and}\]
\[ a_{\mu}=A_{\mu}z\Delta t, b_{\mu}=B_{\mu}z\Delta t, c_{\mu}=C_{\mu}z\Delta t\]
\[d_{\mu}=(A_{\mu}V_{\mu-1}(t)+B_{\mu}V_{\mu}(t)+C_{\mu}V_{\mu+1}(t)+D_{\mu})\Delta t.\]
Using these formulas, update procedure provided in Dayan and Abbot, and Hodgkin-Huxley model, we are able to simulate axon potential propagation. Code for this simulation is provided in Appendix B. 

\subsection*{Problem 3}
Next, we initiated an action potential on one end of the axon by injecting a current in the terminal compartment $\mu=0$. Total cable length was chosen to be $L=1cm$ and the cable was divided to $N=200$ compartments. Compartment length was therefore $L/N=0.01/200m= 50\mu$.

Figure \ref{fig:ex3} shows the action potential progression through the compartments. We can see that it takes approximately 4ms for one action potential to go through one compartment. Figure \ref{fig:ex3} corresponds well with the figure 6.17 presented in Theoretical Neuroscience by Dayan and Abbot. 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{ex3}
    \caption{The first figure shows the action potential at one time step. The second to figures show how the action potential moves through an individual compartment with respect to time.}
    \label{fig:ex3}
\end{figure}

\subsection*{Problem 4}
In this section we determined the action potential propagation velocity as a function of the axon radius. Velocity is defined by computing the time $t_{max}$ when the maximum of the potential occurs at a given spatial location. We then compare $t_{max}$ for two different compartments and compute the velocity by dividing the spatial difference by the time difference. 
\[velocity=\frac{\Delta x}{\Delta t} = \frac{x^2 - x^1}{t_{max}^2 - t_{max}^1}\]

\subsection*{Problem 5}
Next, we initiated action potentials at both ends of the axon. Task was to show that they annihilate when they collide. This is because of the refactory period of both potentials: action potentials are unable to get past the other one.

\subsection*{Problem 6}
In this last section, we simulated action potential propagation in a myelinated axon (see e.g. Chapter 6.4 from Dayan and Abbott, 2001). The nodes of Ranvier (the unmyelinated spaces) are considered to be $2 \mu m$ long and are located at $1 mm$ intervals along the axon. Myelin was considered to increases the resistance across the cell membrane by a factor of 5,000 and decreases the capacitance by a factor of 50.

\end{document}